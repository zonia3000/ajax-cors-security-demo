CREATE DATABASE IF NOT EXISTS cors;

create user cors@localhost identified by 'cors';
grant all privileges on cors.* to cors@localhost;

USE cors;

CREATE TABLE IF NOT EXISTS utente (
    nickname VARCHAR(255) PRIMARY KEY,
    `password` VARCHAR(255),
    descrizione LONGTEXT
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS post (
    id INT AUTO_INCREMENT PRIMARY KEY,
    utente VARCHAR(255),
    testo LONGTEXT,
    data_creazione DATETIME DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (utente) REFERENCES utente(nickname)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert into utente(nickname,`password`) values('xonya',PASSWORD('pippo'));