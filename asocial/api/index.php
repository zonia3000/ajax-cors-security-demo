<?php

require '../vendor/autoload.php';
require 'dao.php';

Flight::route('/', function() {
    echo 'CORS demo API';
});

Flight::route('POST /login', function() {
    $nickname = Flight::request()->data['nickname'];
    $password = Flight::request()->data['password'];

    $dao = new DAO();
    $utente = $dao->login($nickname, $password);

    if ($utente) {
        session_start();
        $_SESSION['utente'] = $utente;
        Flight::halt(200, json_encode($utente));
    } else {
        Flight::halt(401, "Wrong username or password");
    }
});

Flight::route('GET /utente', function() {
    session_start();
    if (isset($_SESSION['utente'])) {
        echo json_encode($_SESSION['utente']);
    } else {
        Flight::halt(401, "Devi fare il login per usare questo endpoint!");
    }
});

Flight::route('PUT /descrizione', function() {
    session_start();
    if (isset($_SESSION['utente'])) {
        $utente = $_SESSION['utente'];
        $descrizione = Flight::request()->data['descrizione'];
        $dao = new DAO();
        $dao->modificaDescrizione($utente->nickname, $descrizione);
        $utente->descrizione = $descrizione;
        $_SESSION['utente'] = $utente;
    } else {
        Flight::halt(401, "Devi fare il login per usare questo endpoint!");
    }
});

function getPosts() {
    $nickname = $_SESSION['utente']->nickname;
    $dao = new DAO();
    $posts = $dao->getPosts($nickname);
    return json_encode($posts);
}

Flight::route('GET /post', function() {
    session_start();
    if (isset($_SESSION['utente'])) {
        echo getPosts();
    }
});

Flight::route('POST /post', function() {
    session_start();
    if (isset($_SESSION['utente'])) {
        $nickname = $_SESSION['utente']->nickname;
        $testo = Flight::request()->data['testo'];

        $canPost = true;
        if (parse_ini_file('../config.ini')['use_token']) {
            $token = Flight::request()->data['token'];
            if ($token !== $_SESSION['token']) {
                $canPost = false;
                Flight::halt(401, "Token non valido!");
            }
        }

        if ($canPost) {
            $dao = new DAO();
            $dao->addPost($nickname, $testo);
            echo getPosts();
        }
    } else {
        Flight::halt(401, "Devi fare il login per usare questo endpoint!");
    }
});

Flight::route('DELETE /post/@id', function($id) {
    session_start();
    if (isset($_SESSION['utente'])) {
        $nickname = $_SESSION['utente']->nickname;
        $dao = new DAO();
        $dao->eliminaPost($nickname, $id);
    } else {
        Flight::halt(401, "Devi fare il login per usare questo endpoint!");
    }
});

Flight::route('GET /logout', function() {
    session_start();
    session_destroy();
});

Flight::route('/echo', function() {
    $valore = Flight::request()->data['value'];
    $risposta = 'ECHO: richiesta ' . Flight::request()->method;
    if (isset($valore)) {
        $risposta .= '. valore=' . $valore;
    }
    echo $risposta;
});

Flight::start();
