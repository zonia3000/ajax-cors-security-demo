<?php

require 'utente.php';
require 'post.php';

class DAO {

    private $db;

    public function __construct() {
        $this->db = new PDO("mysql:host=localhost;dbname=cors", "cors", "cors");
        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    public function login($nickname, $password) {
        $stmt = $this->db->prepare("SELECT descrizione FROM utente WHERE nickname = :nickname AND `password` = PASSWORD(:password);");
        $stmt->bindParam(':nickname', $nickname);
        $stmt->bindParam(':password', $password);
        $stmt->execute();

        if ($stmt->rowCount() === 1) {
            $utente = new Utente();
            $utente->nickname = $nickname;
            $utente->descrizione = $stmt->fetchColumn();
            return $utente;
        }
        return null;
    }

    public function modificaDescrizione($nickname, $descrizione) {
        $stmt = $this->db->prepare("UPDATE utente SET descrizione = :descrizione WHERE nickname = :nickname");
        $stmt->bindParam(':descrizione', $descrizione);
        $stmt->bindParam(':nickname', $nickname);
        $stmt->execute();
    }

    public function getPosts($nickname) {
        $stmt = $this->db->prepare("SELECT id, testo, data_creazione from post WHERE utente = :utente");
        $stmt->bindParam(':utente', $nickname);
        $stmt->execute();

        $posts = [];

        foreach ($stmt->fetchAll() as $row) {
            $post = new Post();
            $post->autore = $nickname;
            $post->id = $row['id'];
            $post->data_creazione = $row['data_creazione'];
            $post->testo = $row['testo'];
            $posts[] = $post;
        }

        return $posts;
    }

    public function addPost($nickname, $testo) {
        $stmt = $this->db->prepare("INSERT INTO post(utente, testo) VALUES(:utente, :testo)");
        $stmt->bindParam(':utente', $nickname);
        $stmt->bindParam(':testo', $testo);
        $stmt->execute();
    }

    public function eliminaPost($nickname, $id) {
        $stmt = $this->db->prepare("DELETE FROM post WHERE utente = :utente AND id = :id");
        $stmt->bindParam(':utente', $nickname);
        $stmt->bindParam(':id', $id);
        $stmt->execute();
    }

}
