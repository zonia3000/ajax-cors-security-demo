var sApp = angular.module('sApp', []);

sApp.controller('MainController', function ($scope, $http) {

    $scope.utente = null;
    $scope.nuovoPost = null;

    $http.get('api/utente').then(setUtente);

    $scope.login = {
        form: {
            nickname: null,
            password: null
        },
        errorMsg: null,
        submit: function () {
            $scope.login.errorMsg = null;
            $http.post('api/login', $scope.login.form)
                    .then(setUtente, function (err) {
                        $scope.login.errorMsg = err.data;
                    });
        }
    };

    $scope.descrizione = {
        inEditing: false,
        toggleEdit: function () {
            $scope.descrizione.inEditing = !$scope.descrizione.inEditing;
        },
        salva: function () {
            $http.put('api/descrizione', {
                descrizione: $scope.utente.descrizione
            }).then(function () {
                $scope.descrizione.inEditing = false;
            });
        }
    };

    $scope.post = {
        posts: [],
        nuovoPost: null,
        aggiungiPost: function () {
            $http.post('api/post', (function () {
                var data = {
                    testo: $scope.post.nuovoPost
                };
                var tokenEl = document.getElementById('token');
                if (tokenEl !== null) {
                    data['token'] = tokenEl.value;
                }
                return data;
            })()).then(function (response) {
                $scope.post.nuovoPost = null;
                $scope.post.posts = response.data;
            });
        },
        eliminaPost: function (post) {
            $http.delete('api/post/' + post.id).then(setPosts);
        }
    };

    $scope.logout = function () {
        $http.get('api/logout').then(setUtente);
    };

    function setUtente(response) {
        $scope.utente = null;
        var utente = response.data;
        if (utente) {
            $scope.utente = utente;
            setPosts();
        }
    }

    function setPosts() {
        $http.get('api/post').then(function (response) {
            $scope.post.posts = response.data;
        });
    }
});