<!doctype html>
<html ng-app="sApp">
    <head>
        <title>A-Social</title>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.10/angular.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous" />
        <script src="js/main.js"></script>
    </head>
    <body ng-controller="MainController">

        <?php
        if (parse_ini_file('config.ini')['use_token']) {
            session_start();
            $_SESSION['token'] = base64_encode(openssl_random_pseudo_bytes(10));
            ?>
            <input type="hidden" id="token" value="<?php echo $_SESSION['token']; ?>" />
        <?php } ?>

        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">A-Social</a>
                </div>                
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="#" ng-click="logout()">Logout</a>
                    </li>
                </ul>
            </div>
        </nav>

        <div class="container">
            <form ng-show="utente === null" ng-submit="login.submit()" class="form-horizontal">
                <div class="form-group">
                    <label for="nickname" class="col-sm-2 control-label">Nickname</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" id="nickname" ng-model="login.form.nickname" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="password" class="col-sm-2 control-label">Password</label>
                    <div class="col-sm-5">
                        <input type="password" class="form-control" id="password" ng-model="login.form.password" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <p class="text-danger">{{login.errorMsg}}</p>
                        <input type="submit" class="btn btn-primary" value="Login" />
                    </div>
                </div>
            </form>

            <div ng-show="utente !== null">
                <div class="jumbotron">
                    <h1>{{utente.nickname}}</h1>
                    <p>
                        <span ng-show="!descrizione.inEditing">{{utente.descrizione}}</span>
                        <a href="#" ng-click="descrizione.toggleEdit()"><span class="glyphicon glyphicon-pencil"></span></a>
                    </p>
                    <div class="input-group" ng-show="descrizione.inEditing">
                        <input type="text" class="form-control" ng-model="utente.descrizione" />
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button" ng-click="descrizione.salva()">
                                <span class="glyphicon glyphicon-floppy-disk"></span>
                            </button>
                        </span>
                    </div>
                </div>

                <h2>Posts ({{post.posts.length}})</h2>
                <ul ng-repeat="p in post.posts">
                    <li>
                        <a href="#" class="text-danger" ng-click="post.eliminaPost(p)"><span class="glyphicon glyphicon-remove"></span></a>
                        {{p.testo}}
                    </li>
                </ul>

                <h3>Scrivi nuovo</h3>
                <textarea class="form-control" ng-model="post.nuovoPost"></textarea>
                <div class="text-center">
                    <button class="btn btn-default" type="button" ng-click="post.aggiungiPost()">
                        Invia
                        <span class="glyphicon glyphicon-send"></span>
                    </button>
                </div>
            </div>
        </div>
    </body>
</html>