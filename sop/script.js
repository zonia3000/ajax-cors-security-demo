var base_url = 'http://izanami/asocial/';

function testEchoGET() {
    fetch(base_url + 'api/echo', {
        method: 'GET'
    }).then(r => r.text())
            .then(console.log);
}
function testEchoPUT() {
    fetch(base_url + 'api/echo', {
        method: 'PUT'
    }).then(r => r.text())
            .then(console.log);
}

function testEchoPOSTFormData() {
    fetch(base_url + 'api/echo', {
        method: 'POST',
        body: (function () {
            var $data = new FormData();
            $data.append('value', 'ciao');
            return $data;
        })()
    }).then(r => r.text())
            .then(console.log);
}

function testEchoPOSTJSON() {
    fetch(base_url + 'api/echo', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({value: 'ciao'})
    }).then(r => r.text())
            .then(console.log);
}

function testUtenteNoAuth() {
    fetch(base_url + 'api/utente', {
        method: 'GET'
    }).then(r => r.text())
            .then(console.log);
}

function testUtenteAuth() {
    fetch(base_url + 'api/utente', {
        method: 'GET',
        credentials: 'include'
    }).then(r => r.text())
            .then(console.log);
}

function testPUTDescrizione() {
    fetch(base_url + 'api/descrizione', {
        method: 'PUT',
        credentials: 'include',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({descrizione: 'Hacked!'})
    }).then(r => r.text())
            .then(console.log);
}
