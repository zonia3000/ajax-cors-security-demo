<?php

require '../vendor/autoload.php';

Flight::route('/', function() {
    echo 'CORS demo API';
});

function getUtente() {
	session_start();
	if(isset($_SESSION['utente'])) {
		return $_SESSION['utente'];
	}
	return 'anonymous';
}

Flight::route('GET /get', function() {
    echo 'GET request from user ' . getUtente();
});
Flight::route('POST /post', function() {
    echo 'POST request from user ' . getUtente() . '. Value=' . Flight::request()->data['value'];
});
Flight::route('PUT /put', function() {
    echo 'PUT request from user ' . getUtente() . '. Value=' . Flight::request()->data['value'];
});
Flight::route('DELETE /delete', function() {
    echo 'DELETE request from user ' . getUtente();
});

Flight::route('POST /login', function() {
    $utente = Flight::request()->data['username'];
	session_start();
	$_SESSION['utente'] = $utente;
});

Flight::route('GET /logout', function() {
    session_start();
    session_destroy();
});

Flight::start();
